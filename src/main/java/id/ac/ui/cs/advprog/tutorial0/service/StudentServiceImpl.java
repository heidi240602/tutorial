package id.ac.ui.cs.advprog.tutorial0.service;
import id.ac.ui.cs.advprog.tutorial0.exception.DuplicateStudentNameException;
import id.ac.ui.cs.advprog.tutorial0.model.Student;
import id.ac.ui.cs.advprog.tutorial0.service.StudentService;
import id.ac.ui.cs.advprog.tutorial0.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentRepository studentRepository;

    @Override
    public Student create(Student student){
        validateName(student);
        generateNPM(student);
        studentRepository.create(student);
        return student;
    }

    private void validateName(Student student){
        List<Student> allStudents=findAll();
        for(Student dbStudent:allStudents){
            if(dbStudent.getName().equals(student.getName())){
                throw new DuplicateStudentNameException(student.getName());
            }
        }
    }

    private void generateNPM(Student student){
        StringBuilder s= new StringBuilder();
        for(char letter: student.getName().toCharArray()){
            s.append(String.valueOf((int)letter));
        }
        String npm=s.toString();
        student.setNpm(npm);
    }

    @Override
    public List<Student> findAll(){
        Iterator<Student> studentIterator=studentRepository.findAll();
        List<Student> allStudents=new ArrayList<Student>();
        studentIterator.forEachRemaining(allStudents::add);
        return allStudents;
    }
}
