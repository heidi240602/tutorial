package id.ac.ui.cs.advprog.tutorial0.controller;

import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
public class BaseController {
    @RequestMapping(method = RequestMethod.GET, path= "/")
    public String index(){

        return "home";
    }

    @GetMapping(path="/greet")
    public String IndexWithParam(@RequestParam("name")String name, Model model){
        model.addAttribute( "name",name);
        return "home";
    }
    @GetMapping(path="/greet/{name}")
    public String IndexWithPathVariable(@PathVariable("name")String name, Model model){
        model.addAttribute( "name",name);
        return "home";
    }

}
