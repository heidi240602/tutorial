package id.ac.ui.cs.advprog.tutorial0.controller;

import id.ac.ui.cs.advprog.tutorial0.exception.DuplicateCourseNameException;
import id.ac.ui.cs.advprog.tutorial0.model.Course;
import id.ac.ui.cs.advprog.tutorial0.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/course")
public class CourseController {
    @Autowired
    private CourseService courseService;

    @GetMapping("/list")
    public String courseList(Model model){
        List<Course> allCourses=courseService.findAll();
        model.addAttribute("courses",allCourses);
        return "courseList";
    }
    @GetMapping("/create")
    public String createCoursePage(Model model){
        Course nCourse=new Course();
        model.addAttribute("course",nCourse);
        return "createCourse";
    }
    @PostMapping("/create")
    public String createStudentPost(@ModelAttribute Course course, Model model){
        try{
            courseService.create(course);
        }
        catch(DuplicateCourseNameException e){
            model.addAttribute("error",e);
            model.addAttribute("course",course);
            return "createCourse";
        }
        return "redirect:list";
    }
}
