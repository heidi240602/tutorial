package id.ac.ui.cs.advprog.tutorial0.service;

import id.ac.ui.cs.advprog.tutorial0.exception.DuplicateCourseNameException;
import id.ac.ui.cs.advprog.tutorial0.model.Course;
import id.ac.ui.cs.advprog.tutorial0.repository.CourseRepository;
import id.ac.ui.cs.advprog.tutorial0.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class CourseServiceImpl implements CourseService {
    @Autowired
    private CourseRepository courseRepository;
    @Override
    public Course create(Course course) {
        validateName(course);
        generateId(course);
        course.setVacant(true);
        courseRepository.createCourse(course);
        return course;
    }
    private void validateName(Course course){
        List<Course> allCourses=findAll();
        for(Course c:allCourses) {
            if (course.getCourseName().equals(c.getCourseName())) {
                throw new DuplicateCourseNameException(course.getCourseName());
            }
        }
    }
    private void generateId(Course course){
        StringBuilder s=new StringBuilder();
        for(char a:course.getCourseName().toCharArray()){
            s.append(String.valueOf((int)a));
        }
        String newCourseID=s.toString();
        course.setCourseID(newCourseID);
    }
    @Override
    public List<Course> findAll() {
        Iterator<Course> courseInMemory=courseRepository.findAll();
        List<Course> allCourses=new ArrayList<Course>();
        courseInMemory.forEachRemaining(allCourses::add);
        return allCourses;
    }
}
