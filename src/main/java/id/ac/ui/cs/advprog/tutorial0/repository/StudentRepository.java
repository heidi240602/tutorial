package id.ac.ui.cs.advprog.tutorial0.repository;
import java.util.*;
import id.ac.ui.cs.advprog.tutorial0.model.Student;
import org.springframework.stereotype.Repository;

@Repository
public class StudentRepository {
    private List<Student> studentsInMemory= new ArrayList<Student>();

    public Student create(Student student){
        studentsInMemory.add(student);
        return student;
    }

    public Iterator<Student> findAll(){
        return studentsInMemory.iterator();
    }
}
